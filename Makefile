PKG=ROI.plugin.optimx
EMAIL=""
R ?= R

readme:
	$(R) -e 'rmarkdown::render("README.Rmd", clean = FALSE)'
	pandoc README.md -s --highlight-style=pygments -o README.html

build:
	$(R) CMD build .

inst: build
	$(R) CMD INSTALL ${PKG}*.tar.gz
	
check: build
	$(R) CMD check ${PKG}*.tar.gz

check_as_cran: build
	$(R) CMD check --as-cran ${PKG}*.tar.gz

manual: clean
	$(R) CMD Rd2pdf --output=Manual.pdf .

clean:
	rm -f Manual.pdf README.knit.md README.html
	rm -rf .Rd2pdf*

devcheck_win_devel: build
	R -e "devtools::check_win_devel(email = 'FlorianSchwendinger@gmx.at')"
